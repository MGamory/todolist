import './App.css';

import React, { useState } from 'react';

import { Box, ChakraProvider, Heading } from '@chakra-ui/react';

import InputField from './components/InputField/InputField';
import TodoList from './components/TodoList/TodoList';
import { Todo } from './models/model';

const App: React.FC = () => {
  const [todo, setTodo] = useState<string>('');
  const [todos, setTodos] = useState<Todo[]>([]);

  const handleAdd = (event: React.FormEvent) => {
    event.preventDefault();
    console.log(event);

    if (todo) {
      setTodos([...todos, { id: Date.now(), todo, isDone: false }]);
      setTodo('');
    }
  };

  return (
    <ChakraProvider>
      <Box
        className='App'
        bgGradient='linear(orange.200, pink.200)'
        w='100%'
        h='100vh'
      >
        <Heading p='5vh'>Taskify</Heading>
        <InputField todo={todo} setTodo={setTodo} handleAdd={handleAdd} />
        <TodoList todos={todos} setTodos={setTodos} />
      </Box>
    </ChakraProvider>
  );
};

export default App;
