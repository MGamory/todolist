import React from 'react';

import { Flex } from '@chakra-ui/react';

import { Todo } from '../../models/model';
import SingleTodo from '../SingleTodo/SingleTodo';

interface Props {
  todos: Todo[];
  setTodos: React.Dispatch<React.SetStateAction<Todo[]>>;
}

const TodoList: React.FC<Props> = ({ todos, setTodos }: Props) => {
  return (
    <Flex direction='row' w='90vw' m='auto' wrap='wrap'>
      {todos.map((todo) => (
        <SingleTodo
          todo={todo}
          todos={todos}
          setTodos={setTodos}
          key={todo.id}
        />
      ))}
    </Flex>
  );
};

export default TodoList;
