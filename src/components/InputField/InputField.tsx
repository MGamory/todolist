import React from 'react';

import { Button, Input } from '@chakra-ui/react';

interface Props {
  todo: string;
  setTodo: React.Dispatch<React.SetStateAction<string>>;
  handleAdd: (event: React.FormEvent) => void;
}

const InputField: React.FC<Props> = ({ todo, setTodo, handleAdd }: Props) => {
  return (
    <form
      className='input'
      onSubmit={(e) => {
        handleAdd(e);
      }}
    >
      <Input
        w='40vw'
        m='10vh'
        variant='flushed'
        placeholder='Enter a task'
        focusBorderColor='pink.500'
        value={todo}
        onChange={(e) => setTodo(e.target.value)}
      />
      <Button className='input__submit' type='submit' colorScheme='pink'>
        Go
      </Button>
    </form>
  );
};

export default InputField;
