import React, { useState } from 'react';
import { AiFillDelete, AiFillEdit } from 'react-icons/ai';
import { MdDone } from 'react-icons/md';

import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Input,
  Stack,
  Tag,
} from '@chakra-ui/react';

import { Todo } from '../../models/model';

interface Props {
  todo: Todo;
  todos: Todo[];
  setTodos: React.Dispatch<React.SetStateAction<Todo[]>>;
}

const SingleTodo = ({ todo, todos, setTodos }: Props) => {
  const [modifying, setModifying] = useState(false);
  const [title, setTitle] = useState('');
  const deleteTodo = () => {
    const newTodos = todos.filter(
      (currentTodo) => todo.todo !== currentTodo.todo
    );
    setTodos(newTodos);
  };

  const doneTodo = () => {
    let newTodo = todo;
    newTodo.isDone = !todo.isDone;
    let newTodos = todos.filter(
      (currentTodo) => todo.todo !== currentTodo.todo
    );
    newTodos.push(newTodo);
    setTodos(newTodos);
  };

  const handleModify = (event: React.FormEvent) => {
    console.log(event.target);
    event.preventDefault();
    let newTodo = todo;
    newTodo.todo = title;
    let newTodos = todos.filter(
      (currentTodo) => todo.todo !== currentTodo.todo
    );
    newTodos.push(newTodo);
    setTodos(newTodos);
    setModifying(!modifying);
  };

  return (
    <Card m='10px'>
      <form onSubmit={handleModify}>
        <CardHeader>
          {modifying ? (
            <Input onChange={(e) => setTitle(e.target.value)} />
          ) : (
            todo.todo
          )}
        </CardHeader>
        <CardBody>
          {todo.isDone ? (
            <Tag mb='10px' colorScheme='teal'>
              Done !
            </Tag>
          ) : (
            <Tag mb='10px' colorScheme='orange'>
              En cours
            </Tag>
          )}

          <Stack direction='row'>
            <Button
              leftIcon={<AiFillEdit />}
              colorScheme='pink'
              onClick={(e) => setModifying(!modifying)}
            >
              Edit
            </Button>
            <Button
              leftIcon={<AiFillDelete />}
              colorScheme='pink'
              onClick={deleteTodo}
            >
              Delete
            </Button>
            <Button leftIcon={<MdDone />} colorScheme='pink' onClick={doneTodo}>
              {todo.isDone ? 'Cancel' : 'Done'}
            </Button>
          </Stack>
        </CardBody>
      </form>
    </Card>
  );
};

export default SingleTodo;
